#pragma strict

var explosion : GameObject; // drag your explosion prefab here
var explosionTime : int = 2;
var explosionSound : AudioClip;

function OnCollisionEnter(collision : Collision){
	if (collision.gameObject.tag == "Enemy" && gameObject.tag == "enemyWeapon") {
		Physics.IgnoreCollision(gameObject.GetComponent.<Collider>(), collision.gameObject.GetComponent.<Collider>());
	} else if (collision.gameObject.tag == "enemyWeapon" && gameObject.tag == "Enemy") {
		Physics.IgnoreCollision(collision.gameObject.GetComponent.<Collider>(), gameObject.GetComponent.<Collider>());
	} else {
	    var explosionClone = Instantiate(explosion, transform.position, Quaternion.identity);
    	GetComponent.<AudioSource>().PlayOneShot(explosionSound, 0.3);
	    Destroy(gameObject);
	    Destroy(explosionClone, explosionTime);
	}
}

@script RequireComponent(AudioSource)