#pragma strict

var weapon : GameObject;

function Update () {
	if (Input.GetMouseButtonDown(0)) {
		var missle : GameObject = Instantiate (weapon, transform.position, transform.rotation);
		missle.GetComponent.<Rigidbody>().AddRelativeForce(Vector3.forward * 1000); 
		// player fired missles should ignores collision with the player
		Physics.IgnoreCollision(missle.GetComponent.<Collider>(), gameObject.GetComponent.<Collider>());
	}
}